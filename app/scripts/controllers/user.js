'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('UserCtrl', function ($rootScope, session) {

    if (!session.isLoggedIn()) {
      $location.path('/login')
    }

    $rootScope.selectedMenu = 'user';

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
