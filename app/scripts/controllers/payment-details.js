'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:PaymentDetailsCtrl
 * @description
 * # PaymentDetailsCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('PaymentDetailsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
