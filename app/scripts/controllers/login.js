'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('LoginCtrl', function ($scope, $rootScope, $http, session, toastr, $log, jwtHelper,
                                     API_SETTINGS, userService, $location) {
    $scope.credentials = {};

    $scope.loginUser = function (credentials) {

      if (credentials.username &&
        credentials.password) {
        $log.info("User logging in ...");

        userService.getOauthToken(credentials).then(
          function (response) {
            console.log(jwtHelper.decodeToken(response.access_token));

            var sessionData = {
              oauth: response,
              credentials: credentials,
              isLoggedIn: true
            };

            session.create(sessionData);

            toastr.success('Welcome! ' + $rootScope.session, 'Login success!');
            $location.path('/home')
          },
          function (errResponse) {
            console.error('Error while loging in user');
          }
        );


        // $http({
        //   method: 'POST',
        //   url: API_SETTINGS.base_url + '/oauth/token',
        //   headers: {
        //     'Content-Type': 'application/x-www-form-urlencoded',
        //     'Authorization': "Basic " + btoa(API_SETTINGS.client_name + ":" + API_SETTINGS.client_password)
        //   },
        //   data: $httpParamSerializerJQLike(data)
        // }).then(function (response) {
        //   var result = response.data;
        //   console.log(jwtHelper.decodeToken(result.access_token));
        //
        //   var sessionData = {
        //     oauth: result,
        //     credentials: credentials,
        //     isLoggedIn: true
        //   };
        //
        //   session.create(sessionData);
        //
        //   toastr.success('Welcome! ' + $rootScope.session, 'Login success!');
        //   $location.path('/home')
        // }, function (response) {
        //
        // });

      } else {
        toastr.error('Username & password is required.', 'Login', {closeButton: true});
      }
    }
  });
