'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
