'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:ChannelDetailsCtrl
 * @description
 * # ChannelDetailsCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('ChannelDetailsCtrl', function ($scope, $location, $log, $http, isNew, API_SETTINGS, $routeParams,toastr) {
    $scope.channel = {};
    $scope.isErr = false;
    $scope.isNew = isNew;
    $scope.errorMessage = '';
    $scope.channel.active = true;

    $scope.createChannel = function () {
      if ($scope.channel.channel_name &&
        $scope.channel.channel_type &&
        $scope.channel.channel_endpoint ) {
        $http({
          method: 'POST',
          data: $scope.channel,
          headers: {
            'Content-Type': 'application/json'
          },
          url: API_SETTINGS.base_url + '/' + API_SETTINGS.api_version + '/channel'
        }).then(function successCallback(response) {
          $scope.errorMessage = 'Saved channel successfully!';
          $log.info($scope.errorMessage);
          $location.path('/channel');
        }, function errorCallback(response) {
          $scope.errorMessage = response.data.message;
          $scope.isErr = true;
          $log.error(response.data);
        });
      }else {
        toastr.error('Please complete all required fields.', 'Mifos Payment Bridge Portal', {closeButton: true});
      }
    };

    $scope.getChannel = function () {
      var channelId = $routeParams.id;
      $http({
        method: 'GET',
        url: API_SETTINGS.base_url + '/' + API_SETTINGS.api_version +'/channel/' + channelId,
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(function (response) {
        $scope.channel = response.data;
        if ($scope.channel.channel_type == 'MOBILE_MONEY_CHANNEL') {
          $scope.channel.channel_type = '1';
        } else if ($scope.channel.channel_type == 'BANKING_CHANNEL') {
          $scope.channel.channel_type = '2';
        } else if ($scope.channel.channel_type == 'EMAIL_MONEY_CHANNEL') {
          $scope.channel.channel_type = '3';
        }
        $scope.isErr = false;
        $log.info($scope.channel);
      }, function (response) {
        $scope.errorMessage = response.data.message;
        $scope.isErr = true;
        $log.error(response.data);
      });
    };

    if (!$scope.isNew) {
      $scope.getChannel();
    }
  });
