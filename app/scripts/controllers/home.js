'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('HomeCtrl', function ($scope, $rootScope, session, $location) {

    if (!session.isLoggedIn()) {
      $location.path('/login')
    }

    $rootScope.selectedMenu = 'home';

    $scope.active = 'home';
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
