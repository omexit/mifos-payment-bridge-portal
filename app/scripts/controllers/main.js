'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('MainCtrl', function ($rootScope, $scope, session, $location) {

    if (!session.isLoggedIn()) {
      $location.path('/login')
    }

    $scope.showNav = function(){
      return session.isLoggedIn();
    };

    $scope.isSelected = function (selectedMenu) {
      return $rootScope.selectedMenu == selectedMenu;
    };

    this.setSelected = function (selectedMenu) {
      $rootScope.selectedMenu = selectedMenu;
    };
  });
