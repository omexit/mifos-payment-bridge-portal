'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:ChannelCtrl
 * @description
 * # ChannelCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('ChannelCtrl', function ($scope, $rootScope, session, channelService) {

    if (!session.isLoggedIn()) {
      $location.path('/login')
    }
    $scope.itemsByPage=25;
    $rootScope.selectedMenu = 'channel';
    $scope.channels = [];

    $scope.getAllChannelList = function () {
      channelService.getAllChannels().then(
        function (response) {
          $scope.channels = response;
        },
        function (errResponse) {
          console.error('Error while getting channel details in user');
        });
    };
    $scope.getAllChannelList();
  });
