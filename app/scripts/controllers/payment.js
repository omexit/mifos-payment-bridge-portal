'use strict';

/**
 * @ngdoc function
 * @name mifosPaymentBridgePortalApp.controller:PaymentCtrl
 * @description
 * # PaymentCtrl
 * Controller of the mifosPaymentBridgePortalApp
 */
angular.module('mifosPaymentBridgePortalApp')
  .controller('PaymentCtrl', function ($rootScope,$scope,session,paymentService) {

    if (!session.isLoggedIn()) {
      $location.path('/login')
    }

    $scope.itemsByPage=25;
    $rootScope.selectedMenu ='payment';
    $scope.payments=[];

    $scope.getAllPayments = function () {
      paymentService.getAllPayments().then(
        function (response) {
          $scope.payments = response;
        },
        function (errResponse) {
          console.error('Error while getting payments');
        });
    };

    $scope.getAllPayments();
  });
