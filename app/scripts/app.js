'use strict';

/**
 * @ngdoc overview
 * @name mifosPaymentBridgePortalApp
 * @description
 * # mifosPaymentBridgePortalApp
 *
 * Main module of the application.
 */
var app = angular
  .module('mifosPaymentBridgePortalApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angular-jwt',
    'toastr',
    'smart-table'
  ]);
app.config(function ($routeProvider, $httpProvider) {
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  delete $httpProvider.defaults.headers.post['Accept'];
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';


  // Intercept http calls.

  // Add the interceptor to the $httpProvider.
  $httpProvider.interceptors.push('HttpInterceptor');

  //Http Intercpetor to check auth failures for xhr requests
  // $httpProvider.interceptors.push(function ($q, $location, $injector,session) {
  //   return {
  //     responseError: function (rejection) {
  //       if (rejection.status <= 0) {
  //         console.log('Error connecting to payment bridge service, request failed! Try again later!');
  //         return $q.reject(rejection);
  //       }
  //
  //       if (rejection.status === 401) {
  //         var deferred = $q.defer(); // defer until we can re-request a new token
  //
  //         var sessionData = session.get();
  //
  //         $injector.get("$http").jsonp(portalConfig.backend + '/authenticate')
  //            .then(function (loginResponse) {
  //                if (loginResponse.data) {
  //                    $injector.get("$http")(response.config).then(function (response) {
  //                        // we have a successful response - resolve it using deferred
  //                        deferred.resolve(response);
  //                    }, function (response) {
  //                        deferred.reject();// something went wrong
  //                    });
  //                } else {
  //                    deferred.reject();// login.json didn't give us data
  //                }
  //            }, function(response) {
  //                deferred.reject(); // token retry failed, redirect so user can login again
  //                 $location.path('/login');
  //                return;
  //            });
  //         return deferred.promise;
  //
  //         $location.path('/login');
  //         return $q.reject(rejection);
  //
  //       }
  //       return $q.reject(rejection);
  //     }
  //   };
  // });

  $routeProvider
    .when('/', {
      templateUrl: 'views/home.html',
      controller: 'HomeCtrl',
      controllerAs: 'home'
    })
    .when('/about', {
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl',
      controllerAs: 'about'
    })
    .when('/login', {
      templateUrl: 'views/login.html',
      controller: 'LoginCtrl',
      controllerAs: 'login'
    })
    .when('/home', {
      templateUrl: 'views/home.html',
      controller: 'HomeCtrl',
      controllerAs: 'home'
    })
    .when('/payment', {
      templateUrl: 'views/payment.html',
      controller: 'PaymentCtrl',
      controllerAs: 'payment'
    })
    .when('/channel', {
      templateUrl: 'views/channel.html',
      controller: 'ChannelCtrl',
      controllerAs: 'channel'
    })
    .when('/user', {
      templateUrl: 'views/user.html',
      controller: 'UserCtrl',
      controllerAs: 'user'
    })
    .when('/payment-details', {
      templateUrl: 'views/payment-details.html',
      controller: 'PaymentDetailsCtrl',
      controllerAs: 'paymentDetails'
    })
    .when('/channel-details', {
      templateUrl: 'views/channel-details.html',
      controller: 'ChannelDetailsCtrl',
      controllerAs: 'channelDetails',
      resolve: {
        'isNew': function () {
          return true;
        }
      }
    }).when('/channel-details/:id', {
    templateUrl: 'views/channel-details.html',
    controller: 'ChannelDetailsCtrl',
    controllerAs: 'channelDetails',
    resolve: {
      'isNew': function () {
        return false;
      }
    }
  }).otherwise({
    redirectTo: '/home'
  });
});
app.run(function () {

});


