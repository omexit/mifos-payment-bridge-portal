'use strict';

/**
 * @ngdoc service
 * @name mifosPaymentBridgePortalApp.channelService
 * @description
 * # channelService
 * Factory in the mifosPaymentBridgePortalApp.
 */
angular.module('mifosPaymentBridgePortalApp')
  .factory('channelService', function ($http, $q, API_SETTINGS, $log) {

    var apiUrl = API_SETTINGS.base_url + '/' + API_SETTINGS.api_version;

    return {
      createChannel: function (channel) {
        return $http({
          method: 'POST',
          url: apiUrl + '/channel',
          headers: {
            'Content-Type': 'application/json'
          },
          data: channel
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while creating channel');
            return $q.reject(errResponse);
          });
      }, getChannelById: function (channelId) {
        return $http({
          method: 'GET',
          url: apiUrl + '/channel/' + channelId,
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while fetching channel with id ' + channelId);
            return $q.reject(errResponse);
          });
      }, getAllChannels: function () {
        return $http({
          method: 'GET',
          url: apiUrl + '/channel',
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while fetching channels');
            return $q.reject(errResponse);
          });
      }, deleteChannel: function (channelId) {
        return $http({
          method: 'DELETE',
          url: apiUrl + '/channel/' + channelId,
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while fetching channel with id ' + channelId);
            return $q.reject(errResponse);
          });
      }
    };
  });
