'use strict';

/**
 * @ngdoc service
 * @name mifosPaymentBridgePortalApp.userService
 * @description
 * # userService
 * Factory in the mifosPaymentBridgePortalApp.
 */
angular.module('mifosPaymentBridgePortalApp')
  .factory('userService', function ($http, $q, API_SETTINGS, $log, $httpParamSerializerJQLike, session) {
    var apiUrl = API_SETTINGS.base_url + '/' + API_SETTINGS.api_version;

    // Public API here
    return {
      createUser: function (user) {
        return $http({
          method: 'POST',
          url: apiUrl + '/user',
          headers: {
            'Content-Type': 'application/json'
          },
          data: user
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while creating user');
            return $q.reject(errResponse);
          });
      }, getAllUsers: function (user) {
        return $http({
          method: 'GET',
          url: apiUrl + '/user',
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while creating user');
            return $q.reject(errResponse);
          });
      }, getUserById: function (userId) {
        return $http({
          method: 'GET',
          url: apiUrl + '/user/' + userId,
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while creating user');
            return $q.reject(errResponse);
          });
      }, getOauthToken: function (credentials) {

        var data = {
          grant_type: 'password',
          username: credentials.username,
          password: credentials.password
        };

        return $http({
          method: 'POST',
          url: API_SETTINGS.base_url + '/oauth/token',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': "Basic " + btoa(API_SETTINGS.client_name + ":" + API_SETTINGS.client_password)
          },
          data: $httpParamSerializerJQLike(data)
        }).then(
          function (response) {
            //Set token in the headers
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while getting oauth token');
            return $q.reject(errResponse);
          });
      }, refreshOauthToken: function () {
        var oauthData = session.getOAUTH();
        var data = {
          grant_type: 'refresh_token',
          client_id: API_SETTINGS.client_name,
          refresh_token: oauthData ? oauthData.refresh_token : null
      }
        ;
        return $http({
          method: 'POST',
          url: API_SETTINGS.base_url + '/oauth/token',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': "Basic " + btoa(API_SETTINGS.client_name + ":" + API_SETTINGS.client_password)
          },
          data: $httpParamSerializerJQLike(data)
        }).then(
          function (response) {
            //Set token in the headers
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while refreshing oauth token');
            return $q.reject(errResponse);
          });
      }, getLoggedInUser: function () {
        return meaningOfLife;
      }
    };
  });
