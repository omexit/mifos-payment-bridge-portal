'use strict';

/**
 * @ngdoc service
 * @name mifosPaymentBridgePortalApp.storage
 * @description
 * # storage
 * Factory in the mifosPaymentBridgePortalApp.
 */
angular.module('mifosPaymentBridgePortalApp')
  .factory('storage', function ($cookies) {
    var expired = new Date();
    expired.setTime(expired.getTime() + (60 * 1000));
    return {
      set: function set(dataKey, data) {
        $cookies.putObject(dataKey, data);
      },
      get: function get(dataKey) {
        return $cookies.getObject(dataKey);
      },
      remove: function remove(dataKey) {
        return $cookies.remove(dataKey);
      }
    };
  });
