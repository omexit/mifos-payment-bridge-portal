'use strict';

/**
 * @ngdoc service
 * @name mifosPaymentBridgePortalApp.config
 * @description
 * # config
 * Constant in the mifosPaymentBridgePortalApp.
 */
angular.module('mifosPaymentBridgePortalApp')
  .constant('API_SETTINGS', {
    base_url: 'http://localhost:2022/api',
    api_version: 'v1',
    client_name: 'client',
    client_password: 'secret'
  })
  .constant('APP_VERSION', '1.0');
