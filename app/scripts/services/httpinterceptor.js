'use strict';

/**
 * @ngdoc service
 * @name mifosPaymentBridgePortalApp.HttpInterceptor
 * @description
 * # HttpInterceptor
 * Factory in the mifosPaymentBridgePortalApp.
 */
angular.module('mifosPaymentBridgePortalApp')
  .factory('HttpInterceptor', function ($q, $injector, $location, session) {

    var userService;
    var http;

    return {
      // On request success
      request: function (config) {
        // console.log(config); // Contains the data about the request before it is sent.
        if (!http) {
          http = $injector.get('$http');
        }
        var oauthData = session.getOAUTH();
        if (oauthData) {
          http.defaults.headers.common['Authorization'] = 'Bearer ' + oauthData.access_token;
        }
        // Return the config or wrap it in a promise if blank.
        return config || $q.when(config);
      },

      // On request failure
      requestError: function (rejection) {
        // console.log(rejection); // Contains the data about the error on the request.

        // Return the promise rejection.
        return $q.reject(rejection);
      },

      // On response success
      response: function (response) {
        // console.log(response); // Contains the data from the response.

        // Return the response or promise.
        return response || $q.when(response);
      },

      // On response failture
      responseError: function (rejection) {

        if (!userService) {
          userService = $injector.get('userService');
        }

        // console.log(rejection); // Contains the data about the error.
        if (rejection.status === 401 && rejection.data.error && rejection.data.error === "invalid_token") {
          var oauthDat = session.getOAUTH();
          session.destroy();
          if (!oauthDat) {
            $location.path('/login')
          } else {
            userService.refreshOauthToken();
          }

        } else if (rejection.status === 401 && rejection.data.error && rejection.data.error === "invalid_token5") {
          session.destroy();
          $location.path('/login')
        }
        // Return the promise rejection.
        return $q.reject(rejection);
      }
    };
  });
