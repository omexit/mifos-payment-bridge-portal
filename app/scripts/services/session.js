'use strict';

/**
 * @ngdoc service
 * @name mifosPaymentBridgePortalApp.session
 * @description
 * # session
 * Factory in the mifosPaymentBridgePortalApp.
 */
angular.module('mifosPaymentBridgePortalApp')
  .factory('session', function (storage) {

    var session_key = '4859886';

    // Public API here
    return {
      create: function (sessionData) {
        storage.set(session_key, sessionData);
      }, destroy: function () {
        storage.remove(session_key);
      }, get: function () {
        return storage.get(session_key);
      }, isLoggedIn: function () {
        var sessiondata = storage.get(session_key);
        return sessiondata ? sessiondata.isLoggedIn : false;
      }, getOAUTH: function () {
        var sessiondata = storage.get(session_key);
        return sessiondata ? sessiondata.oauth : null;
      }
    };
  });
