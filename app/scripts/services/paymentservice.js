'use strict';

/**
 * @ngdoc service
 * @name mifosPaymentBridgePortalApp.paymentService
 * @description
 * # paymentService
 * Factory in the mifosPaymentBridgePortalApp.
 */
angular.module('mifosPaymentBridgePortalApp')
  .factory('paymentService', function ($http, $q, API_SETTINGS, $log) {

    var apiUrl = API_SETTINGS.base_url + '/' + API_SETTINGS.api_version;

    return {
      getAllPayments: function () {
        return $http({
          method: 'GET',
          url: apiUrl + '/payment',
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            $log.error('Error while fetching channels');
            return $q.reject(errResponse);
          });
      }, getAllOutgoingPayments: function () {

      }, getAllIncomingPayments: function () {

      }
    };
  });
