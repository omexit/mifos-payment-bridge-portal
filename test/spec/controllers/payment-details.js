'use strict';

describe('Controller: PaymentDetailsCtrl', function () {

  // load the controller's module
  beforeEach(module('mifosPaymentBridgePortalApp'));

  var PaymentDetailsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PaymentDetailsCtrl = $controller('PaymentDetailsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PaymentDetailsCtrl.awesomeThings.length).toBe(3);
  });
});
