'use strict';

describe('Controller: ChannelDetailsCtrl', function () {

  // load the controller's module
  beforeEach(module('mifosPaymentBridgePortalApp'));

  var ChannelDetailsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ChannelDetailsCtrl = $controller('ChannelDetailsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ChannelDetailsCtrl.awesomeThings.length).toBe(3);
  });
});
